<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC;

use IJsonRPC\Protocol\Factory;
use IJsonRPC\Protocol\Inflector;
use IJsonRPC\Protocol\MethodEnvelope;
use IJsonRPC\Protocol\MethodsCollection;
use IJsonRPC\Protocol\Transport\EndpointCommunicator;

class Client extends Communicator
{
    /**
     * @var Protocol\MethodsCollection
     */
    protected $dumpCollection;

    /**
     * @var array
     */
    protected $dumpCallbacks = [];

    /**
     * @var string
     */
    protected $nsDelimiter = '-';

    /**
     * @var bool
     */
    protected $strictMode = false;

    /**
     * @param EndpointCommunicator $endpointCommunication
     */
    public function __construct(EndpointCommunicator $endpointCommunication = null)
    {
        parent::__construct($endpointCommunication);
        $this->dumpCollection = new MethodsCollection();
    }

    /**
     * @param boolean $strictMode
     */
    public function setStrictMode($strictMode)
    {
        $this->strictMode = (bool) $strictMode;
        $this->endpointCommunication->setStrictMode($this->strictMode);
    }

    /**
     * @return boolean
     */
    public function getStrictMode()
    {
        return $this->strictMode;
    }

    /**
     * @param string $nsDelimiter
     */
    public function setNsDelimiter($nsDelimiter)
    {
        $this->nsDelimiter = $nsDelimiter;
    }

    /**
     * @return string
     */
    public function getNsDelimiter()
    {
        return $this->nsDelimiter;
    }

    /**
     * @param \IJsonRPC\Protocol\MethodsCollection $dumpCollection
     */
    public function setDumpCollection(MethodsCollection $dumpCollection)
    {
        $this->dumpCollection = $dumpCollection;
    }

    /**
     * @return \IJsonRPC\Protocol\MethodsCollection
     */
    public function getDumpCollection()
    {
        return $this->dumpCollection;
    }

    /**
     * @param \IJsonRPC\Protocol\Transport\EndpointCommunicator $endpointCommunication
     */
    public function setEndpointCommunication(EndpointCommunicator $endpointCommunication)
    {
        $this->endpointCommunication = $endpointCommunication;
    }

    /**
     * @return \IJsonRPC\Protocol\Transport\EndpointCommunicator
     */
    public function getEndpointCommunication()
    {
        return $this->endpointCommunication;
    }

    /**
     * @param MethodsCollection $collection
     * @return mixed
     */
    public function flush(MethodsCollection $collection = null)
    {
        $isUserDefined = $collection instanceof MethodsCollection;
        $collection = $collection ? : $this->dumpCollection;

        $this->endpointCommunication->send($collection, $this, $this->strictMode);

        if($isUserDefined) {
            // array_shift is a little overkill and
            // also strict standards warning is thrown
            $methods = $collection->getAllRef();
            reset($methods);
            return current($methods);
        }

        /** @var MethodEnvelope $method */
        foreach($this->dumpCollection->getAllRef() as $method) {
            if(isset($this->dumpCallbacks[$method->getId()]) && $method->hasResult()) {
                call_user_func($this->dumpCallbacks[$method->getId()], $method->getResult()->getValue());
            }
        }

        // clear all
        $this->dumpCallbacks = [];
        $this->dumpCollection = new MethodsCollection();
    }

    /**
     * @param string|array $pseudoMethodString
     * @param array $parameters
     * @throws \UnexpectedValueException
     * @return mixed
     */
    public function call($pseudoMethodString, array $parameters = [])
    {
        if(!is_array($pseudoMethodString)) {
            list($class, $method) = Inflector::parseMethodStringIntoClassAndMethod(
                $pseudoMethodString, $this->nsDelimiter
            );
        } else {
            if(count($pseudoMethodString) < 2) {
                throw new \UnexpectedValueException(
                    "You should provide both class and method name"
                );
            }

            $class = $pseudoMethodString[0];
            $method = $pseudoMethodString[1];
        }

        $collection = new MethodsCollection();
        $method = $this->getFactory()->createMethod($method, $class, $parameters);
        $collection->add($method);

        return $this->flush($collection)->getResult()->getValue();
    }

    /**
     * @param string|array $pseudoMethodString
     * @param array $parameters
     * @param callable $callback
     * @return Protocol\MethodEnvelope
     * @throws \UnexpectedValueException
     */
    public function add($pseudoMethodString, array $parameters = [], callable $callback = null)
    {
        if(!is_array($pseudoMethodString)) {
            list($class, $method) = Inflector::parseMethodStringIntoClassAndMethod(
                $pseudoMethodString, $this->nsDelimiter
            );
        } else {
            if(count($pseudoMethodString) < 2) {
                throw new \UnexpectedValueException(
                    "You should provide both class and method name"
                );
            }

            $class = $pseudoMethodString[0];
            $method = $pseudoMethodString[1];
        }

        $method = $this->getFactory()->createMethod($method, $class, $parameters);
        $this->dumpCollection->add($method);

        if(null !== $callback) {
            $this->dumpCallbacks[$method->getId()] = $callback;
        }

        return $method;
    }

    /**
     * @return \IJsonRPC\Protocol\Factory
     */
    public function getFactory()
    {
        return Factory::getInstance();
    }
}