<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


use IJsonRPC\Protocol\Factory;

class MethodsCollection implements \Countable
{
    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @return int
     */
    public function count()
    {
        return count($this->methods);
    }

    /**
     * @param MethodEnvelopePlaceholder $placeholder
     * @return null|MethodEnvelope
     */
    public function findByPlaceholder(MethodEnvelopePlaceholder $placeholder)
    {
        if($this->exists($placeholder->getId())) {
            return $this->methods[$placeholder->getId()];
        }

        return null;
    }

    /**
     * @return array
     */
    public function & getAllRef()
    {
        return $this->methods;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->methods;
    }

    /**
     * @param mixed $envelope
     * @return null|MethodEnvelope
     */
    public function get($envelope)
    {
        if($this->exists($envelope)) {
            return $this->methods[self::getIdFromMixed($envelope)];
        }

        return null;
    }

    /**
     * @param MethodEnvelope $envelope
     * @throws \RuntimeException
     */
    public function add(MethodEnvelope $envelope)
    {
        if(null === $envelope->getMethod()) {
            throw new \RuntimeException("Method Envelope should have an method defined");
        }

        $this->methods[$envelope->getId()] = $envelope;
    }

    /**
     * @param mixed $envelope
     */
    public function remove($envelope)
    {
        if($this->exists($envelope)) {
            unset($this->methods[self::getIdFromMixed($envelope)]);
        }
    }

    /**
     * @param mixed $envelope
     * @return bool
     */
    public function exists($envelope)
    {
        return isset($this->methods[self::getIdFromMixed($envelope)]);
    }

    /**
     * @param mixed $envelope
     * @return int
     */
    protected static function getIdFromMixed($envelope)
    {
        return ($envelope instanceof MethodEnvelope) ? $envelope->getId() : (int) $envelope;
    }

    /**
     * @param string $string
     * @return MethodsCollection
     * @throws \RuntimeException
     */
    public static function createFromString($string)
    {
        /** @var \IJsonRPC\Protocol\Transport\ConverterDriver\IDriver $converter */
        $converter = Factory::getInstance()->getDefaultConverter();

        if(!$converter->validate($string)) {
            throw new \RuntimeException("Unable to parse given string: {$converter->getLastError()}");
        }

        $data = $converter->decode($string);
        $collection = new self;

        foreach($data as $rawMethod) {
            $collection->add(MethodEnvelope::createFromString($rawMethod));
        }

        return $collection;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $encodedMethods = [];

        foreach($this->methods as $method) {
            $encodedMethods[] = (string) $method;
        }

        return Factory::getInstance()->getDefaultConverter()->encode($encodedMethods);
    }
}