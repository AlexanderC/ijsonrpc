<?php declare(ticks = 1);
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport\EndpointCommunicatorDriver;

use IJsonRPC\Communicator;
use IJsonRPC\Protocol\ExceptionsList;
use IJsonRPC\Protocol\MethodsCollection;
use IJsonRPC\Protocol\ResultSet;

class SocketDriver extends ADriver
{
    const LENGTH_NETWORK_BYTES = 4;

    /**
     * @var int
     */
    protected $maxConnections = 200;

    /**
     * @var float
     */
    protected $connectionTimeout = 0.1;

    /**
     * @param int $maxConnections
     */
    public function setMaxConnections($maxConnections)
    {
        $this->maxConnections = (int)$maxConnections;
    }

    /**
     * @return int
     */
    public function getMaxConnections()
    {
        return $this->maxConnections;
    }

    /**
     * @param float $connectionTimeout
     */
    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connectionTimeout = (float)$connectionTimeout;
    }

    /**
     * @return float
     */
    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    /**
     * @param Communicator $communicator
     * @param bool $returnResult
     * @return string|void
     *
     * @throws \RuntimeException
     */
    public function receiveAndDispatch(Communicator $communicator, $returnResult = false)
    {
        $parts = [];
        $socket = $this->socketCreate($communicator, $parts);
        list(, $address, $port) = $parts;

        if (false === socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1)
            || false === socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1)
            || !socket_bind($socket, $address, $port)
            || !socket_listen($socket, $this->maxConnections)
            || !socket_set_nonblock($socket)
        ) {
            throw new \RuntimeException(
                "Unable to setup socket using {$communicator->getUrl()} as url"
            );
        }

        $clients = [];
        $timeoutSec = floor($this->connectionTimeout);
        $timeoutUsec = $this->connectionTimeout - $timeoutSec;

        $killCallback = function($signal) use ($socket, & $clients) {
            static $killed = false;

            if(!$killed) {
                foreach($clients as $client) {
                    @socket_close($client);
                }
                @socket_close($socket);
                $killed = true;
            }

            exit;
        };

        pcntl_signal(SIGTERM, $killCallback);
        pcntl_signal(SIGINT, $killCallback);
        register_shutdown_function(function() use ($killCallback) {
            $killCallback(SIGTERM);
        });

        for (;;) {
            $read = array_merge([$socket], $clients);

            if (socket_select($read, $write, $except, $timeoutSec, $timeoutUsec)) {
                if (in_array($socket, $read)) {
                    if (($client = @socket_accept($socket))) {
                        $clients[] = $client;
                    } // TODO: manage things when refusing connection
                }
            }

            foreach($clients as $key => $client) {
                if (in_array($client, $read)) {
                    $data = @socket_read(
                        $client,
                        @unpack('Nlen', @socket_read($client, self::LENGTH_NETWORK_BYTES))['len']
                    );

                    $collection = MethodsCollection::createFromString($data);
                    $result = ResultSet::dispatchAndGenerateResponse($collection, $communicator);

                    $packedResultData = pack('N', strlen($result)) . $result;

                    socket_write($client, $packedResultData, strlen($packedResultData));

                    // if all done
                    socket_close($client);
                    $clients[$key] = null;
                }
            }

            $clients = array_filter($clients);
            gc_collect_cycles();
        }
    }

    /**
     * @param MethodsCollection $collection
     * @param Communicator $communicator
     * @return Communicator|void
     *
     * @throws \RuntimeException
     */
    protected function _send(MethodsCollection $collection, Communicator $communicator)
    {
        $parts = [];
        $socket = $this->socketCreate($communicator, $parts);
        list(, $address, $port) = $parts;

        $connection = socket_connect($socket, $address, $port);

        if(false === $connection) {
            throw new \RuntimeException("Unable to connect to IJsonRpc server using {$communicator->getUrl()}");
        }

        $input = (string) $collection;
        $packedInputData = pack('N', strlen($input)) . $input;

        socket_write($socket, $packedInputData, strlen($packedInputData));

        $rawResult = @socket_read(
            $socket,
            @unpack('Nlen', @socket_read($socket, self::LENGTH_NETWORK_BYTES))['len']
        );

        socket_close($socket);

        if (ExceptionsList::isServiceException($rawResult)) {
            if (true === $this->strictMode) {
                throw new \RuntimeException(
                    ExceptionsList::getServiceExceptionName($rawResult) .
                    " Exception thrown while executing on server side"
                );
            }
        }

        $resultSet = new ResultSet();
        $resultSet->fillInternalList($rawResult);
        $resultSet->fillCollection($collection, $this->strictMode);
    }

    /**
     * @param Communicator $communicator
     * @param array $parts
     * @return resource
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    protected function socketCreate(Communicator $communicator, array & $parts = [])
    {
        if (!preg_match("#^(\w+)://([\d.\w]+):(\d+)$#ui", strtolower($communicator->getUrl()), $matches)) {
            throw new \UnexpectedValueException(
                "Unable to parse connection string, please use smth like tcp://127.0.0.1:27015"
            );
        }

        list(, $protocol, $address, $port) = $matches;

        // try to resolve it
        if(!filter_var($address, FILTER_VALIDATE_IP)) {
            $address = @gethostbyname($address);
        }

        $parts = [$protocol, $address, $port];
        unset($matches);

        if ( !in_array($protocol, ['udp', 'tcp'])) {
            throw new \UnexpectedValueException("For socket can only be used both tcp or udp connections");
        }

        // TODO: implement upd protocol and remove this ugly piece of code
        if($protocol !== 'tcp') {
            throw new \UnexpectedValueException("For socket can only be used tcp connection");
        }

        if (!($socket = socket_create(
                AF_INET,
                $protocol === 'udp' ? SOCK_DGRAM : SOCK_STREAM,
                $protocol === 'udp' ? SOL_UDP : SOL_TCP)
            )) {
            throw new \RuntimeException(
                "Unable to create socket using {$communicator->getUrl()} as url"
            );
        }

        return $socket;
    }
}