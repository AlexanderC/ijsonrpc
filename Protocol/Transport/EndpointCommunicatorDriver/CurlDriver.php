<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport\EndpointCommunicatorDriver;

use IJsonRPC\Communicator;
use IJsonRPC\Protocol\ExceptionsList;
use IJsonRPC\Protocol\MethodsCollection;
use IJsonRPC\Protocol\ResultSet;

class CurlDriver extends ADriver
{
    const USER_AGENT = "IJsonRPC lib. v0.1b";
    const TIMEOUT = 10;

    /**
     * @param Communicator $communicator
     * @param bool $returnResult
     * @return string|void
     */
    public function receiveAndDispatch(Communicator $communicator, $returnResult = false)
    {
        try {
            $collection = MethodsCollection::createFromString(file_get_contents("php://input"));
            $result = ResultSet::dispatchAndGenerateResponse($collection, $communicator);
        } catch(\Exception $e) {
            $result = ExceptionsList::RUNTIME_EXCEPTION;
        }

        if($returnResult) {
            return $result;
        }

        $prevErrorReportingState = error_reporting(0);
        @ob_end_clean();
        ob_implicit_flush(1);
        echo $result;
        error_reporting($prevErrorReportingState);
        exit;
    }

    /**
     * @param MethodsCollection $collection
     * @param Communicator $communicator
     * @return Communicator|void
     *
     * @throws \RuntimeException
     */
    protected function _send(MethodsCollection $collection, Communicator $communicator)
    {
        $rawResult = $this->push($communicator->getUrl(), (string) $collection);

        if(ExceptionsList::isServiceException($rawResult)) {
            if(true === $this->strictMode) {
                throw new \RuntimeException(
                    ExceptionsList::getServiceExceptionName($rawResult) .
                    " Exception thrown while executing on server side"
                );
            }
        }

        $resultSet = new ResultSet();
        $resultSet->fillInternalList($rawResult);
        $resultSet->fillCollection($collection, $this->strictMode);
    }

    /**
     * @param string $url
     * @param string $string
     * @return string
     * @throws \RuntimeException
     */
    protected function push($url, $string)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::TIMEOUT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $string);

        if(false === ($data = curl_exec($curl))) {
            throw new \RuntimeException(sprintf("Error %s: %s", curl_errno($curl), curl_error($curl)));
        }

        curl_close($curl);

        return $data;
    }
}