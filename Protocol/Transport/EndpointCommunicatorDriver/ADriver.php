<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport\EndpointCommunicatorDriver;

use IJsonRPC\Communicator;
use IJsonRPC\Protocol\ExceptionsList;
use IJsonRPC\Protocol\MethodEnvelope;
use IJsonRPC\Protocol\MethodsCollection;
use IJsonRPC\Protocol\ResultSet;

abstract class ADriver
{
    /**
     * @var bool
     */
    protected $strictMode = false;

    /**
     * @param bool $strictMode
     */
    public function setStrictMode($strictMode)
    {
        $this->strictMode = $strictMode;
    }

    /**
     * @return bool
     */
    public function getStrictMode()
    {
        return $this->strictMode;
    }

    /**
     * @param mixed $dataObject
     * @param Communicator $communicator
     */
    public function send($dataObject, Communicator $communicator)
    {
        $this->_send(self::getCollectionFromDataObject($dataObject), $communicator);
    }

    /**
     * @param MethodsCollection $collection
     * @param Communicator $communicator
     * @return mixed
     */
    abstract protected function _send(MethodsCollection $collection, Communicator $communicator);

    /**
     * @param Communicator $communicator
     * @param bool $returnResult
     * @return string|void
     */
    abstract public function receiveAndDispatch(Communicator $communicator, $returnResult = false);

    /**
     * @param object $dataObject
     * @return MethodsCollection
     * @throws \RuntimeException
     */
    protected static function getCollectionFromDataObject($dataObject)
    {
        if($dataObject instanceof MethodsCollection) {
            $collection = $dataObject;
        } elseif($dataObject instanceof MethodEnvelope) {
            $collection = new MethodsCollection();
            $collection->add($dataObject);
        } else {
            throw new \RuntimeException("Data object should be both MethodsCollection or MethodEnvelope");
        }

        return $collection;
    }
}