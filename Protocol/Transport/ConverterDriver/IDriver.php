<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport\ConverterDriver;


interface IDriver 
{
    /**
     * @param mixed $data
     * @return string
     */
    public static function encode($data);

    /**
     * @param string $rawData
     * @return mixed
     */
    public static function decode($rawData);

    /**
     * @param string $rawData
     * @return bool
     */
    public static function validate($rawData);

    /**
     * Only returned if validate() method called before
     *
     * @return string
     */
    public static function getLastError();
}