<?php
/**
 * Created by JetBrains PhpStorm.
 * User: AlexanderC
 * Date: 7/22/13
 * Time: 11:26 PM
 * To change this template use File | Settings | File Templates.
 */

namespace IJsonRPC\Protocol\Transport\ConverterDriver;


class JsonDriver implements IDriver
{
    /**
     * @var mixed
     */
    protected static $dump;

    /**
     * @var string
     */
    protected static $signature;

    /**
     * @var string
     */
    protected static $lastError;

    /**
     * @param mixed $data
     * @return string
     */
    public static function encode($data)
    {
        return json_encode($data);
    }

    /**
     * @param string $rawData
     * @return mixed
     */
    public static function decode($rawData)
    {
        if(self::$dump && sha1($rawData) === self::$signature) {
            return self::$dump;
        }

        return json_decode($rawData, true) ? : [];
    }

    /**
     * @param string $rawData
     * @return bool
     */
    public static function validate($rawData)
    {
        $data = json_decode($rawData, true);

        if(null !== $data) {
            self::$dump = $data;
            self::$signature = sha1($rawData);

            return true;
        }

        self::$lastError = json_last_error();

        return false;
    }

    /**
     * @return string
     */
    public static function getLastError()
    {
        return self::$lastError;
    }
}