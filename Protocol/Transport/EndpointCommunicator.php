<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport;

use IJsonRPC\Protocol\Inflector as Inflector;
use IJsonRPC\Protocol\Transport\EndpointCommunicatorDriver\ADriver;

class EndpointCommunicator
{
    const DEFAULT_DRIVER = 'curl';

    /**
     * @var ADriver
     */
    protected $driver;

    /**
     * @param mixed $driver
     * @throws \UnexpectedValueException
     */
    public function __construct($driver = null)
    {
        $driver = $driver ? : self::DEFAULT_DRIVER;
        $this->setDriver($driver);
    }

    /**
     * @param mixed $driver
     * @throws \UnexpectedValueException
     */
    public function setDriver($driver)
    {
        if($driver instanceof ADriver) {
            $this->driver = $driver;
        } else {
            $this->driver = Inflector::buildEndpointCommunicatorDriverClassNameFromString($driver);
            $this->driver = new $this->driver;

            if(!($this->driver instanceof ADriver)) {
                throw new \UnexpectedValueException("{$this->driver} should extend ADriver");
            }
        }
    }

    /**
     * @return ADriver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public function __call($name, array $args)
    {
        return call_user_func_array([$this->driver, $name], $args);
    }
}