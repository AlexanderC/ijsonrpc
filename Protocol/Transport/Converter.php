<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol\Transport;

use IJsonRPC\Protocol\Inflector as Inflector;

/**
 * Class Converter
 *
 * @method string encode(mixed $data)
 * @method mixed decode(string $rawData)
 * @method bool validate(string $rawData)
 * @method string getLastError()
 *
 * @package IJsonRPC\Protocol\Transport
 */
class Converter 
{
    const DEFAULT_DRIVER = 'json';

    /**
     * @var string
     */
    protected $driver;

    /**
     * @param mixed $driver
     * @throws \UnexpectedValueException
     */
    public function __construct($driver = null)
    {
        $driver = $driver ? : self::DEFAULT_DRIVER;
        $this->setDriver($driver);
    }

    /**
     * @param mixed $driver
     * @throws \UnexpectedValueException
     */
    public function setDriver($driver)
    {
        if($driver instanceof ConverterDriver\IDriver) {
            $this->driver = get_class($driver);
        } else {
            $this->driver = Inflector::buildConverterDriverClassNameFromString($driver);

            if(!is_subclass_of(
                $this->driver,
                $driverInterface = sprintf("%s\\ConverterDriver\\IDriver", __NAMESPACE__)
            )) {
                throw new \UnexpectedValueException("{$this->driver} should implement {$driverInterface}");
            }
        }
    }

    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public function __call($name, array $args)
    {
        return call_user_func_array(sprintf("%s::%s", $this->driver, $name), $args);
    }
}