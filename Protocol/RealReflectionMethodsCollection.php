<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class RealReflectionMethodsCollection extends MethodsCollection
{
    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @throws \RuntimeException
     */
    public function add(RealReflectionMethodEnvelope $envelope)
    {
        if(null === $envelope->getMethod()) {
            throw new \RuntimeException("Method Envelope should have an method defined");
        }

        $this->methods[$envelope->getId()] = $envelope;
    }
}