<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;

use IJsonRPC\Helpers\Singleton;
use IJsonRPC\Protocol\Transport\Converter;

class Factory 
{
    use Singleton;

    /**
     * @var Converter
     */
    protected $defaultConverter;

    /**
     * @param \IJsonRPC\Protocol\Transport\Converter $defaultConverter
     */
    public function setDefaultConverter(Converter $defaultConverter)
    {
        $this->defaultConverter = $defaultConverter;
    }

    /**
     * @return \IJsonRPC\Protocol\Transport\Converter
     */
    public function getDefaultConverter()
    {
        return $this->defaultConverter;
    }

    /**
     * @param string $method
     * @param mixed $class
     * @param array $parameters
     * @param Converter $converter
     * @return MethodEnvelope
     */
    public function createMethod($method = null, $class = null, array & $parameters = [], Converter $converter = null)
    {
        $converter = $converter ? : $this->defaultConverter;

        $envelope = new MethodEnvelope($converter);
        $envelope->setRawMethod($method, $class);
        $envelope->setParameters($parameters);

        return $envelope;
    }

    /**
     * @param array $methods
     * @return MethodsCollection
     * @throws \UnexpectedValueException
     */
    public function createMethodsCollection(array & $methods = [])
    {
        $collection = new MethodsCollection();

        foreach($methods as $method) {
            if($method instanceof MethodEnvelope) {
                $collection->add($method);
            } else {
                throw new \UnexpectedValueException("All methods provided should be instances of MethodEnvelope");
            }
        }

        return $collection;
    }

    /**
     * @param MethodsCollection $collection
     * @param bool $cached
     * @return RealReflectionMethodsCollection
     */
    public function convertMethodsCollectionIntoRealReflection(MethodsCollection $collection, $cached = true)
    {
        $reflectionCollection = new RealReflectionMethodsCollection();

        foreach($collection->getAllRef() as $methodEnvelope) {
            $reflectionMethod = (true === $cached)
                ? new CachedRealReflectionMethodEnvelope($methodEnvelope)
                : new RealReflectionMethodEnvelope($methodEnvelope);

            $reflectionCollection->add($reflectionMethod);
        }

        return $reflectionCollection;
    }
}