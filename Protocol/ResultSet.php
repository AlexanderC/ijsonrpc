<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


use IJsonRPC\Communicator;
use IJsonRPC\Helpers\Tree\Node;
use IJsonRPC\Helpers\Tree\Tree;

class ResultSet
{
    const MAX_NESTED_LEVEL = 100;

    /**
     * @var array
     */
    protected $results = [];

    /**
     * @param string $rawData
     * @throws \RuntimeException
     * @throws \UnexpectedValueException
     */
    public function fillInternalList($rawData)
    {
        /** @var \IJsonRPC\Protocol\Transport\ConverterDriver\IDriver $converter */
        $converter = Factory::getInstance()->getDefaultConverter();

        if(!$converter->validate($rawData)) {
            throw new \RuntimeException("Unable to parse given string: {$converter->getLastError()}");
        }

        $data = $converter->decode($rawData);

        if(!self::validateResult($data)) {
            throw new \UnexpectedValueException("Unable to validate result set");
        }

        foreach($data as $field) {
            $result = new Result();
            $result->setId($field['id']);
            $result->setValue($field['result']);

            if(isset($field['exception'])) {
                $result->setException($field['exception']);
            }

            $this->results[$field['id']] = $result;
        }
    }

    /**
     * If strict than exceptions are thrown
     * if something goes wrong, otherwise
     * method resulted with exceptions are
     * not filled with result data
     *
     * @param MethodsCollection $collection
     * @param bool $strict
     * @throws \UnexpectedValueException
     */
    public function fillCollection(MethodsCollection $collection, $strict = false)
    {
        if(true === $strict && count($collection) !== count($this->results)) {
            throw new \UnexpectedValueException("Methods Collection and result set count differ");
        }

        /** @var MethodEnvelope $envelope */
        foreach($collection->getAllRef() as $envelope) {
            if(!isset($this->results[$envelope->getId()])) {
                if(!$strict) continue;

                throw new \UnexpectedValueException("Unable to find Result with id {$envelope->getId()}");
            }

            /** @var Result $result */
            $result = $this->results[$envelope->getId()];

            if(null !== $result->getException()) {
                if(!$strict) continue;

                throw new \UnexpectedValueException(
                    "Method #{$envelope->getId()} resulted with exception: " .
                    ExceptionsList::getServiceExceptionName($result->getException())
                );
            }

            $methodResult = new MethodResult($result->getValue(), $envelope);
            $envelope->setResult($methodResult);
        }
    }

    /**
     * @param MethodsCollection $collection
     * @param Communicator $communicator
     * @return string
     */
    public static function dispatchAndGenerateResponse(MethodsCollection $collection, Communicator $communicator)
    {
        $executionList = [];
        /*$reflectionCollection = Factory::getInstance()
            ->convertMethodsCollectionIntoRealReflection($collection)
            ->getAll();*/

        $collectionMethods = $collection->getAll();

        $callTree = new Tree();

        $reflectionCollectionSize = count($collectionMethods);

        /** @var MethodEnvelope $method */
        foreach($collectionMethods as $key => $method) {
            if(!$method->getParameters()->hasInheritMethod()) {
                $callTree->getHead()->addChild(new Node($method, $method->getId()));
                unset($collectionMethods[$key]);
            }
        }

        $nestedLevel = 0;
        do {
            /** @var MethodEnvelope $method */
            foreach($collectionMethods as $key => $method) {
                if($method->getParameters()->hasInheritMethod()) {
                    $inheritExecutionStack = [];
                    $inheritMethods = 0;
                    $inheritMethodsDispatched = 0;

                    /** @var MethodEnvelopePlaceholder $parameter */
                    foreach($method->getParameters()->getInheritParameters() as $parameter) {
                        $nodes = $callTree->findAll($parameter->getId());

                        if($nodes->count() > 0) {
                            // only store in stack, coz we have to assign children
                            // in transaction only
                            $inheritExecutionStack[] = function() use ($method, $nodes)
                            {
                                $childNode = new Node($method, $method->getId());

                                $nodes->rewind();

                                while($nodes->valid())
                                {
                                     /** @var Node $node */
                                    $node = $nodes->current();

                                    $node->addChild($childNode);

                                    $nodes->next();
                                }
                            };

                            ++$inheritMethodsDispatched;
                        }

                        ++$inheritMethods;
                    }

                    // execute transaction only if all inherit methods
                    // were already added to the call tree to assure
                    // that dependencies are executed first
                    if($inheritMethodsDispatched === $inheritMethods) {
                        foreach($inheritExecutionStack as $closure) {
                            $closure();
                        }

                        unset($collectionMethods[$key]);
                    }
                }
            }

            // oh dude...
            if($nestedLevel >= self::MAX_NESTED_LEVEL) {
                return ExceptionsList::MAX_NESTED_LEVEL_EXCEEDED;
            }

            ++$nestedLevel;
        } while(!empty($collectionMethods));

        do {
            foreach($callTree->getIterator() as $callNode) {
                /** @var MethodEnvelope $reflectionMethodEnvelope */
                $methodEnvelope = $callNode->getData();

                // leave this if already executed
                if($methodEnvelope->hasResult()) {
                    continue;
                }

                $result = [
                    'id' => $methodEnvelope->getId(),
                    'result' => null
                ];

                try {
                    $reflectionMethodEnvelope = new CachedRealReflectionMethodEnvelope($methodEnvelope);
                } catch(\ReflectionException $e) {
                    // the worst thing can happen
                    $result["exception"] = ExceptionsList::METHOD_NOT_ALLOWED;
                    $executionList[] = $result;
                    continue;
                }

                $reflection = $reflectionMethodEnvelope->getReflection();

                if(!$communicator->isMethodAllowed($reflection['class'], $reflection['name'])) {
                    $result["exception"] = ExceptionsList::METHOD_NOT_ALLOWED;
                    $executionList[] = $result;
                    continue;
                }

                try {
                    $parameters = $reflectionMethodEnvelope->getPreparedCallParameters();

                    $filledParameters = [];
                    foreach($parameters as $key => $parameter) {
                        if($parameter instanceof MethodEnvelopePlaceholder
                            || $parameter instanceof MethodEnvelope) {
                            $node = $callTree->find($parameter->getId());

                            $nodeResult = $node->getData()->getResult();

                            // oh... we have not yet this result (((
                            if(!($nodeResult instanceof MethodResult)) {
                                continue 2;
                            }

                            if(null !== $nodeResult->getException()) {
                                $result["exception"] = ExceptionsList::EXCEPTION_INHERIT_METHOD;
                                $executionList[] = $result;
                                continue;
                            }

                            $filledParameters[$key] = $nodeResult->getValue();
                        } else {
                            $filledParameters[$key] = $parameter;
                        }
                    }

                    unset($parameters);
                } catch(\Exception $e) {
                    $result["exception"] = ExceptionsList::WRONG_PARAMETERS;
                    $executionList[] = $result;
                    continue;
                }

                if(!$reflection['isPublic']) {
                    $result["exception"] = ExceptionsList::METHOD_IS_PRIVATE;
                    $executionList[] = $result;
                    continue;
                }

                if(!$reflection['isStatic'] && !$communicator->existsObject($reflection['class'])) {
                    $result["exception"] = ExceptionsList::MISSING_OBJECT_FOR_NON_STATIC_METHOD;
                    $executionList[] = $result;
                    continue;
                }

                $exceptionWhileExecuting = false;

                try {
                    if($reflection['isStatic']) {
                        $result["result"] = call_user_func_array(
                            sprintf("%s::%s", $reflection['class'], $reflection['name']),
                            $filledParameters
                        );
                    } else {
                        $result["result"] = call_user_func_array(
                            [$communicator->getObject($reflection['class']), $reflection['name']],
                            $filledParameters
                        );
                    }
                } catch(\Exception $e) {
                    $exceptionWhileExecuting = true;
                    $result["exception"] = ExceptionsList::EXCEPTION_WHILE_EXECUTING;
                    $result["result"] = null;
                }

                $methods = $callTree->findAll($reflectionMethodEnvelope->getId());

                foreach($methods as $node) {
                    $method = $node->getData();

                    $methodResult = new MethodResult($result["result"], $method);

                    if(true === $exceptionWhileExecuting) {
                        $methodResult->setException(ExceptionsList::EXCEPTION_WHILE_EXECUTING);
                    }

                    $method->setResult($methodResult);
                }

                $executionList[] = $result;
            }
        } while(count($executionList) !== $reflectionCollectionSize);

        /** @var \IJsonRPC\Protocol\Transport\ConverterDriver\IDriver $converter */
        $converter = Factory::getInstance()->getDefaultConverter();

        return $converter->encode($executionList);
    }

    /**
     * @param $data
     * @return bool
     */
    protected static function validateResult($data)
    {
        if(!is_array($data)) return false;

        foreach($data as $field) {
            if(!isset($field['id']) || !array_key_exists('result', $field)) {
                return false;
            }
        }

        return true;
    }
}