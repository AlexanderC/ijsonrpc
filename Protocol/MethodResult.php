<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class MethodResult 
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var MethodEnvelope|RealReflectionMethodEnvelope
     */
    protected $envelope;

    /**
     * @var null|string
     */
    protected $exception = null;

    /**
     * @param mixed $value
     * @param MethodEnvelope|RealReflectionMethodEnvelope $method
     * @throws \RuntimeException
     */
    public function __construct($value, $method)
    {
        if(!($method instanceof MethodEnvelope)
            && !($method instanceof RealReflectionMethodEnvelope)) {
            throw new \RuntimeException(
                "Provided method should be both RealReflectionMethodEnvelope or MethodEnvelope"
            );
        }

        $this->envelope = $method;
        $this->value = $value;
    }

    /**
     * @param null|string $exception
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return null|string
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return MethodEnvelope|RealReflectionMethodEnvelope
     */
    public function getMethodEnvelope()
    {
        return $this->envelope;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return gettype($this->value);
    }
}