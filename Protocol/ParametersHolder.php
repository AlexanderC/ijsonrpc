<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class ParametersHolder implements \Iterator, \ArrayAccess, \Countable
{
    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @return bool
     */
    public function hasInheritMethod()
    {
        foreach($this->parameters as $parameter) {
            if($this->isInheritParameter($parameter)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function & getInheritParameters()
    {
        $inherit = [];

        foreach($this->parameters as $parameter) {
            if($this->isInheritParameter($parameter)) {
                $inherit[] = $parameter;
            }
        }

        return $inherit;
    }

    /**
     * @param mixed $parameter
     * @return bool
     */
    public function isInheritParameter($parameter)
    {
        return ($parameter instanceof MethodEnvelope)
        || ($parameter instanceof MethodEnvelopePlaceholder);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->parameters);
    }

    /**
     * @return mixed|void
     */
    public function rewind()
    {
        return reset($this->parameters);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return false !== current($this->parameters);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        return key($this->parameters);
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return current($this->parameters);
    }

    /**
     * @return mixed|void
     */
    public function next()
    {
        return next($this->parameters);
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_scalar($offset)) {
            $this->parameters[$offset] = $value;
        } else {
            $this->parameters[] = $value;
        }
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->parameters);
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->parameters[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->parameters[$offset] : null;
    }

    /**
     * @return array
     */
    public function getRawArray()
    {
        return $this->parameters;
    }

    /**
     * @param array $input
     */
    public function fromEncodedArray(array & $input)
    {
        foreach($input as $key => $value) {
            if(false === ($tryUnserializedData = @unserialize($value))) { // case inherit method
                $this->offsetSet($key, new MethodEnvelopePlaceholder(Inflector::getMethodEnvelopeIdFromUid($value)));
            } else {
                $this->offsetSet($key, $tryUnserializedData);
            }
        }
    }

    /**
     * @return array
     */
    public function & toEncodedArray()
    {
        $result = array();

        foreach($this->parameters as $key => $value) {
            if($value instanceof MethodEnvelope) { // it's a reference
                $result[$key] = Inflector::buildMethodEnvelopeUid($value);
            } else {
                $result[$key] = serialize($value);
            }
        }

        return $result;
    }
}