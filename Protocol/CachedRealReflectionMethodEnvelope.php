<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;

use IJsonRPC\Cache\Driver\IDriver;
use IJsonRPC\Cache\Factory as CacheFactory;

class CachedRealReflectionMethodEnvelope extends RealReflectionMethodEnvelope
{
    /**
     * @var \IJsonRPC\Cache\Driver\IDriver
     */
    protected $cache;

    /**
     * @param MethodEnvelope $proxy
     * @param IDriver $cache
     */
    public function __construct(MethodEnvelope $proxy, IDriver $cache = null)
    {
        $this->cache = $cache ? : CacheFactory::getDefault();
        parent::__construct($proxy);
    }

    /**
     * @return void
     */
    protected function populateReflection()
    {
        $this->cache->invalidateIfUpdated($this);

        if($this->cache->exists($this)) {
            $this->reflection = $this->cache->get($this);
        } else {
            parent::populateReflection();
            $this->cache->add($this);
        }
    }
}