<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class Inflector 
{
    /**
     * @param string $string
     * @return array|bool
     */
    public static function extractConverterAndInfoFromString($string)
    {
        if(false !== ($pos = strrpos($string, "|"))) {
            return [
                mb_substr($string, 0, $pos),
                self::buildConverterDriverClassNameFromString(mb_substr($string, $pos + 1))
            ];
        }

        return false;
    }

    /**
     * @param $string
     * @param $converter
     * @return string
     */
    public static function buildConverterSpecifiedSerializedString($string, $converter)
    {
        return sprintf("%s|%s", $string, self::separateConverterDriverFromClass($converter));
    }

    /**
     * @param mixed $class
     * @param string $method
     * @return string
     */
    public static function buildMethodStringFromClassAndMethod($class, $method)
    {
        return sprintf("%s::%s", str_replace("\\", "-", is_object($class) ? get_class($class) : $class), $method);
    }

    /**
     * @param string $methodString
     * @param string $nsDelimiter
     * @return array|bool
     */
    public static function parseMethodStringIntoClassAndMethod($methodString, $nsDelimiter = '-')
    {
        $quotedNsDelimiter = preg_quote($nsDelimiter, "/");

        if(preg_match("/^([\w{$quotedNsDelimiter}_]*)::([\w_]+)$/ui", $methodString, $matches)
            && count($matches) === 3) {
            return [
                str_replace($nsDelimiter, "\\", $matches[1]),
                $matches[2]
            ];
        }

        return false;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function buildCacheDriverClassNameFromString($string)
    {
        $ns = "IJsonRPC\\Cache\\Driver\\";

        if(!preg_match("/^" . preg_quote($string, "/") . "\w+Driver$/u", $string)) {
            return sprintf("%s%sDriver", $ns, ucfirst($string));
        }

        return $string;
    }

    /**
     * Note that you should user driver class name
     * or converter class object in order to get correct result
     *
     * @param mixed $class
     * @return null|string
     */
    public static function separateConverterDriverFromClass($class)
    {
        $class = is_object($class) ? $class->getDriver() : $class;

        if(false !== ($pos = strrpos($class, "\\"))) {
            return mb_strtolower(preg_replace("/Driver$/ui", "", mb_substr($class, ++$pos)));
        }

        return null;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function buildEndpointCommunicatorDriverClassNameFromString($string)
    {
        $ns = sprintf("%s\\Transport\\EndpointCommunicatorDriver\\", __NAMESPACE__);

        if(!preg_match("/^" . preg_quote($ns, "/") . "\w+Driver$/u", $string)) {
            return sprintf("%s%sDriver", $ns, ucfirst($string));
        }

        return $string;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function buildConverterDriverClassNameFromString($string)
    {
        $ns = sprintf("%s\\Transport\\ConverterDriver\\", __NAMESPACE__);

        if(!preg_match("/^" . preg_quote($ns, "/") . "\w+Driver$/u", $string)) {
            return sprintf("%s%sDriver", $ns, ucfirst($string));
        }

        return $string;
    }

    /**
     * @param MethodEnvelope $envelope
     * @return string
     */
    public static function buildMethodEnvelopeUid(MethodEnvelope $envelope)
    {
        return sprintf(":%s:", $envelope->getId());
    }

    /**
     * @param string $uid
     * @return null|int
     */
    public static function getMethodEnvelopeIdFromUid($uid)
    {
        if(preg_match("/^:(\d+):$/ui", $uid, $matches) && count($matches) === 2) {
            return (int) $matches[1];
        }

        return null;
    }
}