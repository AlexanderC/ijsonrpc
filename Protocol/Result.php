<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class Result 
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var null|string
     */
    protected $exception = null;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null|string $exception
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return null|string
     */
    public function getException()
    {
        return $this->exception;
    }
}