<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class ExceptionsList 
{
    // thrown when method is private
    const METHOD_IS_PRIVATE = 0x001;
    // thrown if object for non static call not registered on server
    const MISSING_OBJECT_FOR_NON_STATIC_METHOD = 0x002;
    // thrown if wrong parameters provided
    const WRONG_PARAMETERS = 0x003;
    // thrown if both method is not registered or not found
    const METHOD_NOT_ALLOWED = 0x004;
    // thrown when exception thrown while executing given method
    const EXCEPTION_WHILE_EXECUTING = 0x005;
    // thrown when could not resolve dependencies
    const MAX_NESTED_LEVEL_EXCEEDED = 0x006;
    // thrown in special case, if global exception thrown while doing server job
    const RUNTIME_EXCEPTION = 0x007;
    // thrown when one of inherit methods from parameters give an exception
    const EXCEPTION_INHERIT_METHOD = 0x008;

    /**
     * @param mixed $value
     * @return bool
     */
    public static function isServiceException($value)
    {
        return in_array((int) $value, self::getSelfReflection()->getConstants());
    }

    /**
     * @param mixed $value
     * @return string
     */
    public static function getServiceExceptionName($value)
    {
        return array_search((int) $value, self::getSelfReflection()->getConstants(), true);
    }

    /**
     * @return \ReflectionClass
     */
    protected static function getSelfReflection()
    {
        static $reflection;

        if(!isset($reflection)) {
            $reflection = new \ReflectionClass(get_class());
        }

        return $reflection;
    }
}