<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;


class RealReflectionMethodEnvelope
{
    /**
     * @var array
     */
    protected $reflection = [];

    /**
     * @var MethodEnvelope
     */
    protected $proxy;

    /**
     * @param MethodEnvelope $proxy
     */
    public function __construct(MethodEnvelope $proxy)
    {
        $this->proxy = $proxy;
        $this->populateReflection();
    }

    /**
     * @return array
     * @throws \RuntimeException
     */
    public function getPreparedCallParameters()
    {
        $remainingParameters = [];
        $alreadyAddedKeys = [];
        $result = [];
        $defaultsRemaining = $this->reflection['defaultsCount'];

        foreach($this->reflection['parameters'] as $position => $internalParameter) {
            foreach($this->proxy->getParameters() as $name => $parameter) {
                if($name === $internalParameter['name']) {
                    $result[$position] = $parameter;

                    if(array_key_exists($name, $remainingParameters)) {
                        unset($remainingParameters[$name]);
                    }

                    if(isset($internalParameter['default'])) {
                        --$defaultsRemaining;
                    }

                    $alreadyAddedKeys[] = $name;
                } elseif(!in_array($name, $alreadyAddedKeys)) {
                    $remainingParameters[$name] = $parameter;
                }
            }
        }
        unset($alreadyAddedKeys);

        $missingNamedParameters = array_diff_key($this->reflection['parameters'], $result);

        if(($defaultsDiff = (count($missingNamedParameters) - $defaultsRemaining))
            > ($countRemaining = count($remainingParameters))) {
            throw new \RuntimeException("Not enough parameters provided for calling this method");
        }

        $remainingNonDefaults = $countRemaining - $defaultsDiff;
        $remainingNonDefaults = $remainingNonDefaults < 0 ? 0 : $remainingNonDefaults;

        reset($remainingParameters);
        foreach($missingNamedParameters as $position => $internalParameter) {
            if(array_key_exists('default', $internalParameter)) { // case default exists
                if($remainingNonDefaults > 0) {
                    $parameter = current($remainingParameters);
                    next($remainingParameters);
                    --$remainingNonDefaults;
                } else {
                    $parameter = $internalParameter['default'];
                }
            } else {
                $parameter = current($remainingParameters);
                next($remainingParameters);
            }

            $result[$position] = $parameter;

        }
        unset($remainingParameters);
        unset($missingNamedParameters);

        ksort($result, SORT_NUMERIC);

        return $result;
    }

    /**
     * @return void
     */
    protected function populateReflection()
    {
        list($class, $name) = Inflector::parseMethodStringIntoClassAndMethod($this->proxy->getMethod());
        $reflection = new \ReflectionMethod($class, $name);

        $parametersRaw = $reflection->getParameters();
        $parameters = [];
        $defaultsPositions = [];

        /** @var \ReflectionParameter $parameter */
        foreach($parametersRaw as $parameter) {
            $parameters[$parameter->getPosition()] = [
                'name' => $parameter->getName()
            ];

            if($parameter->isDefaultValueAvailable()) {
                $parameters[$parameter->getPosition()]['default'] = $parameter->getDefaultValue();
                $defaultsPositions[] = $parameter->getPosition();
            }
        }
        unset($parametersRaw);

        $this->reflection = [
            'count' => count($parameters),
            'parameters' => $parameters,
            'class' => $class,
            'name' => $name,
            'isPublic' => $reflection->isPublic(),
            'isStatic' => $reflection->isStatic(),
            'defaultsPositions' => $defaultsPositions,
            'defaultsCount' => count($defaultsPositions),
            'file' => $reflection->getFileName()
        ];
    }

    /**
     * @return array
     */
    public function getReflection()
    {
        return $this->reflection;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        return call_user_func_array([$this->proxy, $name], $arguments);
    }
}