<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;

use IJsonRPC\Protocol\Transport\Converter as Converter;

class MethodEnvelope 
{
    const VERSION = '1.0b';

    /**
     * @var Converter
     */
    protected $converter;

    /**
     * @var string
     */
    protected $method = null;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var ParametersHolder
     */
    protected $parameters;

    /**
     * @var MethodResult
     */
    protected $result;

    /**
     * @param Converter $converter
     */
    public function __construct(Converter $converter)
    {
        $this->converter = $converter;
        $this->parameters = new ParametersHolder();
    }

    /**
     * @return bool
     */
    public function hasResult()
    {
        return $this->result instanceof MethodResult;
    }

    /**
     * @param MethodResult $result
     */
    public function setResult(MethodResult $result)
    {
        $this->result = $result;
    }

    /**
     * @return MethodResult
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return Converter
     */
    public function getConverter()
    {
        return $this->converter;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = new ParametersHolder();

        foreach($parameters as $key => $value) {
            $this->addParameter($value, $key);
        }
    }

    /**
     * @param mixed $value
     * @param null|string $key
     */
    public function addParameter($value, $key = null)
    {
        $this->parameters->offsetSet($key, $value);
    }

    /**
     * @param ParametersHolder $holder
     */
    public function setParametersHolder(ParametersHolder $holder)
    {
        $this->parameters = $holder;
    }

    /**
     * @return ParametersHolder
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
        $this->id = $this->getIncrementalUid();
    }

    /**
     * @param $method
     * @param null $class
     */
    public function setRawMethod($method, $class = null)
    {
        $this->setMethod(Inflector::buildMethodStringFromClassAndMethod($class, $method));
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    protected function getIncrementalUid()
    {
        static $counter = 0;
        return (int) (($counter++) . crc32($this->method));
    }

    /**
     * @param string $string
     * @return MethodEnvelope
     * @throws \RuntimeException
     */
    public static function createFromString($string)
    {
        $info = Inflector::extractConverterAndInfoFromString($string);

        if(false === $info) {
            throw new \RuntimeException("Unable to parse incoming data");
        }

        list($string, $converterClass) = $info;
        unset($info);

        $converter = new Converter($converterClass);

        if(!$converter->validate($string)) {
            throw new \RuntimeException("Unable to parse given string: {$converter->getLastError()}");
        }

        $data = $converter->decode($string);

        if(!self::validateRawData($data)) {
            throw new \RuntimeException("Unable to validate incoming data");
        }

        /** @var MethodEnvelope $method */
        $method = Factory::getInstance()->createMethod();

        $parametersHolder = new ParametersHolder();
        $parametersHolder->fromEncodedArray($data['params']);
        $method->setParametersHolder($parametersHolder);
        $method->setMethod($data['method']);
        $method->setId($data['id']); // important to keep it after setting the method

        return $method;
    }

    /**
     * @param mixed $data
     * @return bool
     */
    protected static function validateRawData($data)
    {
        return is_array($data)
            && isset(
                    $data["ijsonrpc"],
                    $data["method"],
                    $data["params"],
                    $data["id"]
                )
            && $data["ijsonrpc"] === self::VERSION
        ;
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function __toString()
    {
        if(empty($this->method)) {
            throw new \RuntimeException("Method can not be null");
        }

        return Inflector::buildConverterSpecifiedSerializedString($this->converter->encode([
            "ijsonrpc" => self::VERSION,
            "method" => $this->method,
            "params" => $this->parameters->toEncodedArray(),
            "id" => $this->id
        ]), $this->converter);
    }

    /**
     * @inherit
     */
    public function __clone()
    {
        $this->id = $this->getIncrementalUid();
        $this->parameters = clone $this->parameters;
    }
}