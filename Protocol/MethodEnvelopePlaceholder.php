<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Protocol;

class MethodEnvelopePlaceholder 
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var MethodResult
     */
    protected $result;

    /**
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function hasResult()
    {
        return $this->result instanceof MethodResult;
    }

    /**
     * @param \IJsonRPC\Protocol\MethodResult $result
     */
    public function setResult(MethodResult $result)
    {
        $this->result = $result;
    }

    /**
     * @return \IJsonRPC\Protocol\MethodResult
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param MethodEnvelope $envelope
     * @return bool
     */
    public function isRelatedToMethodEnvelope(MethodEnvelope $envelope)
    {
        return $envelope->getId() === $this->getId();
    }

    /**
     * @param MethodEnvelope $envelope
     * @return MethodEnvelopePlaceholder
     */
    public static function createFromMethodEnvelope(MethodEnvelope $envelope)
    {
        return new self($envelope->getId());
    }
}