<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC;

use IJsonRPC\Protocol\Transport\EndpointCommunicator;

abstract class Communicator
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var Protocol\Transport\EndpointCommunicator
     */
    protected $endpointCommunication;

    /**
     * @param EndpointCommunicator $endpointCommunication
     */
    public function __construct(EndpointCommunicator $endpointCommunication = null)
    {
        $this->endpointCommunication = $endpointCommunication ? : new EndpointCommunicator();
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}