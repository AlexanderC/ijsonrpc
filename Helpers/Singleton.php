<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Helpers;


trait Singleton 
{
    /**
     * No more constructor...
     */
    final private function __construct()
    {   }

    /**
     * @return Singleton
     */
    public static function getInstance()
    {
        static $self;

        if(!isset($self)) {
            $self = new self;
            call_user_func_array([$self, '__onAfterConstruct'], func_get_args());
        }

        return $self;
    }

    /**
     * Method called after first instantiation
     *
     * @return void
     */
    protected function __onAfterConstruct(/* list of parameters */)
    {   }
}