<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Helpers\Tree;

class Tree
{
    /**
     * @var Node
     */
    protected $head;

    /**
     * @param Node $head
     */
    public function __construct(Node $head = null)
    {
        $this->head = $head ? : new Node('HEAD');
    }

    /**
     * @return Node
     */
    public function getHead()
    {
        return $this->head;
    }

    /**
     * @param mixed $uid
     * @return Node|bool
     */
    public function find($uid)
    {
        /** @var Node $node */
        foreach($this->getIterator() as $node) {
            if($node->getUid() === $uid) {
                return $node;
            }
        }

        return false;
    }

    /**
     * @param mixed $uid
     * @return \SplStack
     */
    public function & findAll($uid)
    {
        $result = new \SplStack();

        /** @var Node $node */
        foreach($this->getIterator() as $node) {
            if($node->getUid() === $uid) {
                $result->push($node);
            }
        }

        return $result;
    }

    /**
     * @return \RecursiveIteratorIterator
     */
    public function getIterator()
    {
        return new \RecursiveIteratorIterator(
            $this->head->getChildren(),
            \RecursiveIteratorIterator::SELF_FIRST
        );
    }
}