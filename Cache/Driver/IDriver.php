<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Cache\Driver;

use IJsonRPC\Protocol\RealReflectionMethodEnvelope;

interface IDriver
{
    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return bool
     */
    public function add(RealReflectionMethodEnvelope $envelope);

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return void
     */
    public function invalidate(RealReflectionMethodEnvelope $envelope);

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return bool
     */
    public function exists(RealReflectionMethodEnvelope $envelope);

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return \ReflectionClass
     */
    public function get(RealReflectionMethodEnvelope $envelope);

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return void
     */
    public function invalidateIfUpdated(RealReflectionMethodEnvelope $envelope);
}