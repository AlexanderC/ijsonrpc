<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Cache\Driver;

use IJsonRPC\Protocol\RealReflectionMethodEnvelope;

class FileDriver implements IDriver
{
    /**
     * @var string
     */
    protected static $cacheDir;

    /**
     * @param string $dir
     */
    public static function setCacheDir($dir)
    {
        self::$cacheDir = realpath($dir);
    }

    /**
     * @return string
     */
    public static function getCacheDir()
    {
        return self::$cacheDir;
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return bool
     */
    public function add(RealReflectionMethodEnvelope $envelope)
    {
        $exportedData = var_export($envelope->getReflection(), true);
        $string = sprintf("<?php /** Generate automatically at %s */\nreturn %s;\n", date("d M Y [h:i:s]"), $exportedData);

        return @file_put_contents(self::getFileName($envelope), $string, LOCK_EX);
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return void
     */
    public function invalidate(RealReflectionMethodEnvelope $envelope)
    {
        @unlink(self::getFileName($envelope));
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return bool
     */
    public function exists(RealReflectionMethodEnvelope $envelope)
    {
        return is_file(self::getFileName($envelope));
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return \ReflectionClass
     */
    public function get(RealReflectionMethodEnvelope $envelope)
    {
        return require(self::getFileName($envelope));
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return void
     */
    public function invalidateIfUpdated(RealReflectionMethodEnvelope $envelope)
    {
        if($this->exists($envelope)) {
            $reflection = $this->get($envelope);

            if(filemtime($reflection['file']) > filemtime(self::getFileName($envelope))) {
                $this->invalidate($envelope);
            }
        }
    }

    /**
     * @param RealReflectionMethodEnvelope $envelope
     * @return string
     */
    protected static function getFileName(RealReflectionMethodEnvelope $envelope)
    {
        $dir = self::$cacheDir ? : sys_get_temp_dir();
        $dir = rtrim(str_replace("\\", "/", $dir), "/");
        $reflection = $envelope->getReflection();

        return sprintf("%s/__iJsonRPC_cache|%s.ptmp", $dir, $envelope->getMethod());
    }
}