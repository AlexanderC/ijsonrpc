<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC\Cache;

use IJsonRPC\Cache\Driver\IDriver;
use IJsonRPC\Helpers\Singleton;
use IJsonRPC\Protocol\Inflector;

/**
 * Class Factory
 * @package IJsonRPC\Cache
 */
class Factory 
{
    use Singleton;

    const DEFAULT_DRIVER = 'file';

    /**
     * @var IDriver
     */
    protected static $default;

    /**
     * @param null|string $driver
     * @return IDriver
     */
    public function create($driver = null)
    {
        $class = Inflector::buildCacheDriverClassNameFromString($driver ? : self::DEFAULT_DRIVER);
        return new $class;
    }

    /**
     * @param \IJsonRPC\Cache\Driver\IDriver $default
     */
    public static function setDefault(IDriver $default)
    {
        self::$default = $default;
    }

    /**
     * @return \IJsonRPC\Cache\Driver\IDriver
     */
    public static function getDefault()
    {
        if(!isset(self::$default)) {
            self::$default = self::getInstance()->create();
        }

        return self::$default;
    }
}