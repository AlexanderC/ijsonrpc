<?php
/**
 * @author AlexanderC
 */

require_once __DIR__ . "/init.php";

class Test
{
    /**
     * @param int $param1
     * @param array $param2
     * @return mixed
     */
    public function test1($param1, $param2)
    {
        return $param1 + $param2;
    }
}

use \IJsonRPC\Protocol\Inflector as Infl;

$converter = new \IJsonRPC\Protocol\Transport\Converter();
\IJsonRPC\Protocol\Factory::getInstance($converter);

$data = [10, 'param2' => 20, 'param1' => 30];
$rightData = [30, 20];

$rawData = $converter->encode($data);
if(!$converter->validate($rawData)) {
    exit("Not valid converted string, error: " . $converter->getLastError());
} elseif($data !== $converter->decode($rawData)) {
    exit("Unable to revert converted data");
}

//var_dump(Infl::separateConverterDriverFromClass($converter->getDriver()));
$envelope = new \IJsonRPC\Protocol\MethodEnvelope($converter);
$envelope->setRawMethod("test1", "Test");
$envelope->setParameters($data);

$envUid = Infl::buildMethodEnvelopeUid($envelope);
$envId = Infl::getMethodEnvelopeIdFromUid($envUid);

$envelope2 = clone $envelope;
$envelope2->setRawMethod("test1", "Test");
$envelope2->addParameter($envelope);

$params = $envelope2->getParameters();
$encParams = $params->toEncodedArray();

$params2 = new \IJsonRPC\Protocol\ParametersHolder();
$params2->fromEncodedArray($encParams);

if(count($params) !== count($params2)) {
    exit("Ups, smth getting bad with envelop or params holder");
}

$collection = new \IJsonRPC\Protocol\MethodsCollection();
$collection->add($envelope);
$collection->add($envelope2);

if(count($collection) != 2) {
    exit("Ups, smth wents wrong with Methods Collection");
}

$sendString = (string) $collection;

$collectionParsed = \IJsonRPC\Protocol\MethodsCollection::createFromString($sendString);

if(count($collection) !== count($collectionParsed)) {
    exit("Ups, seems that collection are not imported correctly");
}

$reflEnvelope = new \IJsonRPC\Protocol\CachedRealReflectionMethodEnvelope($envelope);
$preparedParams = $reflEnvelope->getPreparedCallParameters();

if($preparedParams !== $rightData) {
    exit("Smth goes wrong with parameters preparation");
}

$server = new \IJsonRPC\Server();
$server->addAllowedMethod(new \ReflectionMethod("Test", "test1"));
$server->registerObject(new Test());
//$server->dispatch();

$envelopeInherit1 = clone $envelope;
$envelopeInherit1->setParameters([10, $envelope]);

$envelopeInherit2 = clone $envelope;
$envelopeInherit2->setParameters([$envelopeInherit1, $envelope]);

$envelopeInherit3 = clone $envelope;
$envelopeInherit3->setParameters([$envelopeInherit2, 30]);

$envelopeInherit4 = clone $envelope;
$envelopeInherit4->setParameters([$envelope2, 30]);

$collectionInheritTest = new \IJsonRPC\Protocol\MethodsCollection();
$collectionInheritTest->add($envelopeInherit2);
$collectionInheritTest->add($envelopeInherit1);
$collectionInheritTest->add($envelopeInherit3);
$collectionInheritTest->add($envelope);

$identicalCollectionServerSide = \IJsonRPC\Protocol\MethodsCollection::createFromString((string) $collectionInheritTest);

$response = \IJsonRPC\Protocol\ResultSet::dispatchAndGenerateResponse($identicalCollectionServerSide, $server);

if(count(json_decode($response, true)) !== count($collectionInheritTest)) {
    exit("Smth wents wrong while dispatching end returning server result");
}

exit("Tests done, no errors.");
