<?php
/**
 * @author AlexanderC
 */

require_once __DIR__ . "/init.php";

class Test
{
    /**
     * @param int $param1
     * @param array $param2
     * @return mixed
     */
    public function test1($param1, $param2)
    {
        return $param1 + $param2;
    }
}

$converter = new \IJsonRPC\Protocol\Transport\Converter();
\IJsonRPC\Protocol\Factory::getInstance()->setDefaultConverter($converter);

$communicator = new IJsonRPC\Protocol\Transport\EndpointCommunicator('socket');

$server = new \IJsonRPC\Server($communicator);
$server->setUrl("tcp://127.0.0.1:27015");
$server->addAllowedMethod("Test::test1");
$server->registerObject(new Test());
$server->dispatch();

