<?php
/**
 * @author AlexanderC
 */

spl_autoload_register(function ($class) {
    $dir = realpath(__DIR__ . "/../../");

    if(file_exists($file = sprintf("%s/%s.php", $dir, str_replace("\\", "/", $class)))) {
        require $file;
        return true;
    }

    return false;
});