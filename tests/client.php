<?php
/**
 * @author AlexanderC
 */

require_once __DIR__ . "/init.php";

$converter = new \IJsonRPC\Protocol\Transport\Converter();
\IJsonRPC\Protocol\Factory::getInstance()->setDefaultConverter($converter);

$communicator = new IJsonRPC\Protocol\Transport\EndpointCommunicator('socket');

$client = new \IJsonRPC\Client($communicator);
$client->setUrl("tcp://localhost:27015");
$client->setStrictMode(true);

// test simple call
$result1 = $client->call("Test::test1", [10, 20]);

if($result1 !== 30) {
    exit("Heh, wrong result of sum. Expected (integer) 30 got (" . gettype($result1) . ") {$result1}");
}

// test simple inheritance
$envelopeTest1 = $client->add("Test::test1", [10, 20]);
$client->add("Test::test1", [10, $envelopeTest1], function($result) {
    if($result !== 40) {
        exit("Heh, wrong result of inheritance test. Expected (integer) 40 got (" . gettype($result) . ") {$result}");
    }
});
$client->flush();

// test missing method
try {
    $result1 = $client->call("Test::test2", [10, 20]);

    exit("UnexpectedValueException should be thrown if method not allowed");
} catch (\UnexpectedValueException $e) {    }

// test unresolved dependency
try {
    $result2 = $client->call("Test::test1", [10, $envelopeTest1]);

    exit("RuntimeException should be thrown if dependencies could not be resolved");
} catch (\RuntimeException $e) {    }

exit("Tests done, no errors.");