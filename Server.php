<?php
/**
 * @author AlexanderC
 */

namespace IJsonRPC;

use IJsonRPC\Protocol\Inflector;
use IJsonRPC\Protocol\Transport\EndpointCommunicator;

class Server extends Communicator
{
    /**
     * @var array
     */
    protected $registeredObjects = [];

    /**
     * @var array
     */
    protected $allowedMethods = [];

    /**
     * @param bool $return
     * @return mixed
     */
    public function dispatch($return = false)
    {
        return $this->endpointCommunication->receiveAndDispatch($this, $return);
    }

    /**
     * @param string|array $pseudoMethodString
     * @throws \UnexpectedValueException
     */
    public function addAllowedMethod($pseudoMethodString)
    {
        if(!is_array($pseudoMethodString)) {
            list($class, $method) = Inflector::parseMethodStringIntoClassAndMethod(
                $pseudoMethodString, $this->nsDelimiter
            );
        } else {
            if(count($pseudoMethodString) < 2) {
                throw new \UnexpectedValueException(
                    "You should provide both class and method name"
                );
            }

            $class = $pseudoMethodString[0];
            $method = $pseudoMethodString[1];
        }

        $this->addAllowedMethodReflection(new \ReflectionMethod($class, $method));
    }

    /**
     * @param \ReflectionMethod $method
     * @throws \RuntimeException
     */
    public function addAllowedMethodReflection(\ReflectionMethod $method)
    {
        if(!$method->isPublic()) {
            throw new \RuntimeException("Unable to add non public method as allowed");
        }

        $this->allowedMethods[] = $method;
    }

    /**
     * @param string $class
     * @param string $name
     * @return bool
     */
    public function isMethodAllowed($class, $name)
    {
        /** @var \ReflectionMethod $method */
        foreach($this->allowedMethods as $method) {
            if($method->getDeclaringClass()->getName() === $class
            && $method->getName() === $name) return true;
        }

        return false;
    }

    /**
     * @return void
     */
    public function clearAllowedMethods()
    {
        $this->allowedMethods = [];
    }

    /**
     * @param array $allowedMethods
     */
    public function setAllowedMethods(array $allowedMethods)
    {
        $this->allowedMethods = $allowedMethods;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return $this->allowedMethods;
    }

    /**
     * @return \IJsonRPC\Protocol\Transport\EndpointCommunicator
     */
    public function getEndpointCommunication()
    {
        return $this->endpointCommunication;
    }

    /**
     * @param mixed $object
     * @return null|object
     */
    public function getObject($object)
    {
        if($this->existsObject($object)) {
            return $this->registeredObjects[is_object($object) ? get_class($object): (string) $object];
        }

        return null;
    }

    /**
     * @param object $object
     */
    public function registerObject($object)
    {
        if(!$this->existsObject($object)) {
            $this->registeredObjects[get_class($object)] = $object;
        }
    }

    /**
     * @param mixed $object
     * @return bool
     */
    public function existsObject($object)
    {
        return isset($this->registeredObjects[is_object($object) ? get_class($object): (string) $object]);
    }

    /**
     * @param array $registeredObjects
     */
    public function setRegisteredObjects(array $registeredObjects)
    {
        $this->registeredObjects = $registeredObjects;
    }

    /**
     * @return array
     */
    public function getRegisteredObjects()
    {
        return $this->registeredObjects;
    }
}